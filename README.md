# Phonebook to Google CSV

A small tool that formats [Phonerlite's](http://phonerlite.de/index_de.htm) ```phonebook.csv``` into the Google contact file format, so you can then import your contacts into google.

### Arguments

    usage: csvtogoogle [-h] [-f FILE] [-l LABEL]

    formats Phonerlite's phonebook.csv into the Google contact file format

    options:
    -h, --help              show this help message and exit
    -f FILE, --file FILE    input file
    -l LABEL, --label LABEL set a label e.g. 'Work ::: * myContacts'
