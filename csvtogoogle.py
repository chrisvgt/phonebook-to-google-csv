import pandas as pd
import argparse

parser = argparse.ArgumentParser(
    description="formats Phonerlite's phonebook.csv into the Google contact file format")
parser.add_argument("-f", "--file", type=argparse.FileType("r"), dest="FILE", help="input file")
parser.add_argument("-l", "--label", type=str, dest="LABEL", help="set a label e.g. 'Work ::: * myContacts'")
args = parser.parse_args()

numbers = []
names = []
if (args.LABEL != ""):
    LABEL = args.LABEL

df1 = pd.read_csv(args.FILE.name, sep=";", header=0)
len_df1 = len(df1)
for i in range(len_df1):
    change = False
    number = df1.iloc[i, 0]
    if number.find("@fritz.box"):
        check = True
        number = number.replace("@fritz.box", "")
    if number[0] == "0":
        change = True
        number = "+49" + number[1:]
    if change == True:
        df1.iat[i, 0] = number
    numbers.append(number)
    names.append(df1.iloc[i, 1])

data = {"Name": names, "Given Name": names,
        "Group Membership": args.LABEL, "Phone 1 - Value": numbers}
col_names = ["Name", "Given Name", "Additional Name", "Family Name", "Yomi Name", "Given Name Yomi", "Additional Name Yomi", "Family Name Yomi", "Name Prefix", "Name Suffix", "Initials", "Nickname", "Short Name", "Maiden Name", "Birthday",
             "Gender", "Location", "Billing Information", "Directory Server", "Mileage", "Occupation", "Hobby", "Sensitivity", "Priority", "Subject", "Notes", "Language", "Photo", "Group Membership", "Phone 1 - Type", "Phone 1 - Value"]
df2 = pd.DataFrame(data, columns=col_names)
df2.to_csv("contacts.csv", index=False)
